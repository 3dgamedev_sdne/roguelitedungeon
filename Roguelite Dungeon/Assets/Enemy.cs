﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public float _CurrentHealth = 10;
    public float _SightDistance = 10;

    private Animator anim;
    private NavMeshAgent agent;

    public Vector3 initPos;
    public Vector3 playerPos;

    private void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        initPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (_CurrentHealth == 0)
        {
            Destroy(gameObject);
        }

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Attack")) { agent.isStopped = true; }
        else
        {
            agent.isStopped = false;
            playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;

            if ((playerPos - transform.position).magnitude <= _SightDistance)
            {
                agent.destination = playerPos;
            }
            else
            {
                agent.destination = initPos;
            }

            if (agent.remainingDistance >= agent.stoppingDistance)
            {
                anim.SetBool("Moving", true);
            }
            else
            {
                anim.SetBool("Moving", false);
            }
        }
    }

    public void Attack()
    {
        anim.SetTrigger("Attack");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Sword>())
        {
            if (other.gameObject.GetComponent<Sword>().tag == "Sword" || other.gameObject.GetComponent<Sword>().tag == "Axe")
            {
                _CurrentHealth = _CurrentHealth - 2;
            }
        }
    }
}
