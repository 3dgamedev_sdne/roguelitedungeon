﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    public MenuClassifier HomeScreen;

    public bool tutorial = false;
    public bool sound = true;

    private List<AudioListener> listeners;

    public void ExitGame()
    {

        SceneLoader.Instance.UnloadScene(SceneManager.GetActiveScene().path);
        // UNLOAD ART SCENE EVENTUALLY

        MenuManager.Instance.HideAllMenus();
        // REPLACE WITH GAMEOVER SCREEN
        MenuManager.Instance.ShowMenu(HomeScreen); 
    }

    public void ToggleSound()
    {

        listeners = new List<AudioListener>(FindObjectsOfType<AudioListener>());

        if (listeners.Count > 0)
        {
            if (sound)
            {
                foreach (AudioListener l in listeners)
                {
                    l.enabled = false;
                }
                GameManager.Instance.sound = false;
            }
            else
            {
                foreach (AudioListener l in listeners)
                {
                    l.enabled = true;
                }
                GameManager.Instance.sound = false;
            }
        }
    }

}
