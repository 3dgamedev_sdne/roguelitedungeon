﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DynamicLevelGenerator : Singleton<DynamicLevelGenerator>
{

    public List<GameObject> layouts;
    //public List<GameObject> stages;
    public List<GameObject> spawnPoints;
    //public List<GameObject> centerPoints;
    public List<GameObject> NPCs;
    private GameObject NPC;
    public List<GameObject> items;
    private GameObject item;
    public GameObject player;

    /*
    public Dictionary<int, List<GameObject>> degenPoints;
    
    public List<GameObject> dpLayer1;
    public List<GameObject> dpLayer2;
    public List<GameObject> dpLayer3;
    public List<GameObject> dpLayer4;


    // Temporary values for debug purposes
    
    int startRoom;
    int endRoom;

    int nextOpening;
    */
    int numOfNPCs = 5;
    int numOfItems = 5;


    // Start is called before the first frame update
    void Start()
    {
        
        /*
        int rand = Random.Range(0, spawnPoints.Count);

        spawnPoints.Remove(spawnPoints[rand]);
        */

        Spawn();

    }

    public void Spawn()
    {
        if (GameManager.Instance.tutorial == false)
        {
            int randL = Random.Range(0, layouts.Count);
            Instantiate(layouts[randL]);
        }


        spawnPoints = new List<GameObject>(GameObject.FindGameObjectsWithTag("SpawnPoint"));

        if (GameObject.FindGameObjectWithTag("PlayerSpawn"))
        {
            GameObject p = Instantiate(player, GameObject.FindGameObjectWithTag("PlayerSpawn").transform);
            p.transform.parent = null;
        }

        for (int i = 0; i < numOfNPCs; i++)
        {
            int rand = Random.Range(0, spawnPoints.Count);
            int randNPC = Random.Range(0, NPCs.Count);

            NPC = NPCs[randNPC];
            GameObject npc = Instantiate(NPC, spawnPoints[rand].transform);
            //npc.GetComponent<NPCMovement>().npcId = i + 1;
            npc.transform.parent = null;
            spawnPoints.Remove(spawnPoints[rand]);
            //NPCs.Remove(NPCs[randNPC]);
        }

        for (int j = 0; j < numOfItems; j++)
        {
            int rand = Random.Range(0, spawnPoints.Count);
            int randItem = Random.Range(0, items.Count);

            item = items[randItem];
            GameObject it = Instantiate(item, spawnPoints[rand].transform);
            it.transform.parent = null;
            spawnPoints.Remove(spawnPoints[rand]);
        }
    }
}
