﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ConfigScreenController : Menu
{
    public MenuClassifier HUDClassifier;
    public MenuClassifier HomeClassifier;

    public SceneReference LevelScene;
    public SceneReference TutorialScene;


    public override void OnHideMenu(string options)
    {
        base.OnHideMenu(options);
    }

    public override void OnShowMenu(string options)
    {
        base.OnShowMenu(options);
    }



    #region Testing


    public void OnSoundSelected()
    {
        GameManager.Instance.ToggleSound();
    }

    public void OnBackSelected()
    {

        MenuManager.Instance.HideMenu(menuClassifier);
        MenuManager.Instance.ShowMenu(HomeClassifier);
    }

    void SceneLoadedCallback(List<string> scenesLoaded)
    {
        SceneLoader.Instance.sceneLoadedEvent.RemoveListener(SceneLoadedCallback);
        MenuManager.Instance.ShowMenu(HUDClassifier);
    }

    #endregion
}
