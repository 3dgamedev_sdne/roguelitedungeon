﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HomeScreenController : Menu
{
    public MenuClassifier HUDClassifier;
    public MenuClassifier storyClassifier;
    public MenuClassifier configClassifier;

    public SceneReference LevelScene;
    public SceneReference TutorialScene;

    public override void OnHideMenu(string options)
    {
        base.OnHideMenu(options);
    }

    public override void OnShowMenu(string options)
    {
        base.OnShowMenu(options);
    }



    #region Testing


    public void OnLevelSelected()
    {
        /*SceneLoader.Instance.sceneLoadedEvent.AddListener(SceneLoadedCallback);
        List<string> scenes = new List<string>();

        scenes.Add(LevelScene);

        SceneLoader.Instance.LoadScenes(scenes);*/
        GameManager.Instance.tutorial = false;

        MenuManager.Instance.HideMenu(menuClassifier);
        MenuManager.Instance.ShowMenu(storyClassifier);
    }

    public void OnTutorialSelected()
    {
        GameManager.Instance.tutorial = true;

        SceneLoader.Instance.sceneLoadedEvent.AddListener(SceneLoadedCallback);
        List<string> scenes = new List<string>();

        scenes.Add(TutorialScene);

        SceneLoader.Instance.LoadScenes(scenes);

        MenuManager.Instance.HideMenu(menuClassifier);
    }

    public void OnQuitSelected()
    {
        Application.Quit();
    }

    public void OnConfigSelected()
    {
        MenuManager.Instance.HideMenu(menuClassifier);
        MenuManager.Instance.ShowMenu(configClassifier);
    }

    void SceneLoadedCallback(List<string> scenesLoaded)
    {
        SceneLoader.Instance.sceneLoadedEvent.RemoveListener(SceneLoadedCallback);
        //MenuManager.Instance.ShowMenu(HUDClassifier);
    }

    #endregion
}
