﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StoryScreenController : Menu
{
    public MenuClassifier HUDClassifier;
    public MenuClassifier HomeClassifier;

    public SceneReference LevelScene;
    public SceneReference TutorialScene;

    public override void OnHideMenu(string options)
    {
        base.OnHideMenu(options);
    }

    public override void OnShowMenu(string options)
    {
        base.OnShowMenu(options);
    }



    #region Testing


    public void OnLevelSelected()
    {
        SceneLoader.Instance.sceneLoadedEvent.AddListener(SceneLoadedCallback);
        List<string> scenes = new List<string>();

        scenes.Add(LevelScene);

        SceneLoader.Instance.LoadScenes(scenes);

        MenuManager.Instance.HideMenu(menuClassifier);
    }

    public void OnBackSelected()
    {

        MenuManager.Instance.HideMenu(menuClassifier);
        MenuManager.Instance.ShowMenu(HomeClassifier);
    }

    void SceneLoadedCallback(List<string> scenesLoaded)
    {
        SceneLoader.Instance.sceneLoadedEvent.RemoveListener(SceneLoadedCallback);
        MenuManager.Instance.ShowMenu(HUDClassifier);
    }

    #endregion
}
