﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName ="MenuClassifier", menuName = "Create/Custom Menu Classifier")]
public class MenuClassifier : ScriptableObject
{
    public string menuName;
}
