﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EquippableItemPickUpDetector : MonoBehaviour
{
    public CollisionRelay pickUpColliderRelay;
    public Collider mainCollider;
    public BasePickUpAble pickUp;

    // Start is called before the first frame update
    void Start()
    {
        pickUpColliderRelay.OnTriggerEnterEvent += OnPickUpTriggerEnter;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("EquippableItemPickUpDetector: Update");

    }

    public void OnPickUpTriggerEnter(Collider other)
    {
        Debug.Log("EquippableItemPickUpDetector: OnPickUpTriggerEnter");

        Transform t = other.transform;
        KnightController knightController = null;
        while (t != null)
        {
            if (t.gameObject.GetComponent<KnightController>() != null)
            {
                knightController = t.gameObject.GetComponent<KnightController>();
                break;
            }
            t = t.parent.transform;
        }



        if (knightController != null)
        {
            Debug.Log("EquippableItemPickUpDetector: OnPickUpTriggerEnter: other.gameObject.GetComponent<KnightController>");
            GameObject playerGameObj = other.gameObject;

            pickUpColliderRelay.SetColliderEnabled(false);
            pickUp.PickUp(playerGameObj);
        }

    }

}

public enum PickUpSize
{
    LARGE_WEAPON,
    THROWABLE
}