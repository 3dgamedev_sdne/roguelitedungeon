﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BasePickUpAble : MonoBehaviour
{
    public abstract void PickUp(GameObject pickerUpper);
}
