﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseWeapon : BasePickUpAble
{
    public WeaponSlotType slotType;

    public abstract void Activate(PlayerWeaponManager weaponManager);

    public abstract void OnEquip();

    public abstract void OnUnequip();

    public abstract void AnimationEvent(PlayerWeaponManager weaponManager);

    public override void PickUp(GameObject pickerUpper)
    {
        Debug.Log("WPNPCK: BaseWeapon: PickUp()");

        if (pickerUpper.GetComponent<PlayerWeaponManager>() != null)
        {
            PlayerWeaponManager weaponManager = pickerUpper.GetComponent<PlayerWeaponManager>();
            weaponManager.PickUpWeapon(this);

        } 


    }
}
