﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSlot : MonoBehaviour
{
    public bool IgnoreOwnership;
    public BaseWeapon weapon;

    public void SetSlotOwner(BaseWeapon w)
    {
        if (!IgnoreOwnership)
        {
            weapon = w;
        }
    }

    public void ClearSlotOwner()
    {
        weapon = null;
    }

    public bool SlotHasOwner()
    {
        if (!IgnoreOwnership)
        {
            return weapon != null;
        }
        else
        {
            return false;
        }
    }

    public BaseWeapon GetSlotOwner()
    {
        if (!IgnoreOwnership)
        {
            return weapon;
        }
        else
        {
            return null;
        }
        
    }
}
