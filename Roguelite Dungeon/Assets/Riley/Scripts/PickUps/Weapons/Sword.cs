﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sword : BaseWeapon
{
    public Transform rightHandOffset;
    public float _CurrentHealth = 10;
    public float _MaxHealth = 10;
    public Slider HealthBar;

    public override void Activate(PlayerWeaponManager weaponManager)
    {
        KnightController.instance.gameObject.GetComponent<Animator>().SetTrigger("Attack");
    }

    public override void AnimationEvent(PlayerWeaponManager weaponManager)
    {
        throw new System.NotImplementedException();
    }

    public override void OnEquip()
    {
        transform.position = rightHandOffset.position;
        transform.rotation = rightHandOffset.rotation;
        Destroy(GetComponent("AutoMoveAndRotate"));
        HealthBar.value = calculateHealth();
    }

    public override void OnUnequip()
    {
        if (_CurrentHealth == 0)
        {
            Destroy(gameObject);
            HealthBar.value = 0;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        HealthBar.value = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (_CurrentHealth == 0)
        {
            OnUnequip();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Enemies")
        {
            Debug.Log("Collided with " + other.gameObject.ToString());
            _CurrentHealth--;
            HealthBar.value = calculateHealth();
        }
    }
    public float calculateHealth()
    {
        return _CurrentHealth / _MaxHealth;
    }
}
