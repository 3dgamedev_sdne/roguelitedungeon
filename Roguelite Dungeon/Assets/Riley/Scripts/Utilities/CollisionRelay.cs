﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollisionRelay : MonoBehaviour
{
    public delegate void ColliderEvent(Collider other);
    public ColliderEvent OnTriggerEnterEvent;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("CollisionRelay: OnTriggerEnter");

        if (OnTriggerEnterEvent != null)
        {
            OnTriggerEnterEvent.Invoke(other);
        }
    }

    public void SetColliderEnabled(bool isEnabled)
    {
        GetComponent<Collider>().enabled = isEnabled;
    }
}
