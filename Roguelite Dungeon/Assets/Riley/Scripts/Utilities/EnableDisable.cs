﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableDisable : MonoBehaviour
{
    public Camera camera;
    private bool hasActivated;

    // Start is called before the first frame update
    void Start()
    {
        camera.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasActivated)
        {
            camera.enabled = true;
            hasActivated = true;
        }

    }
}
