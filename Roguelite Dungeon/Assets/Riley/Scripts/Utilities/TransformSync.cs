﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformSync : MonoBehaviour
{
    public Transform transformToSyncFrom;

    // Update is called once per frame
    void Update()
    {
        transform.position = transformToSyncFrom.position;
        transform.rotation = transformToSyncFrom.rotation;
    }
}
