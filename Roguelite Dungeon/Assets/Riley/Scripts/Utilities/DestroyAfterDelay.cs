﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterDelay : MonoBehaviour
{
    public float timeToLive;

    private float timeToLiveTimer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timeToLiveTimer += Time.deltaTime;

        if(timeToLiveTimer > timeToLive)
        {
            Destroy(gameObject);
        }
    }
}
