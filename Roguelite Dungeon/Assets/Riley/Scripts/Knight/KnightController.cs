﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KnightController : MonoBehaviour
{
    public PlayerWeaponManager weaponManager;

    public static KnightController instance;

    public float health = 100;

    public Slider playerHealth;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }

        if (GameManager.Instance.sound == false)
        {
            List<AudioListener> listeners = new List<AudioListener>(FindObjectsOfType<AudioListener>());

            if (listeners.Count > 0)
            {
                foreach (AudioListener l in listeners)
                {
                    l.enabled = false;
                }
            }
        }
    }

    private void Start()
    {
        playerHealth.value = calculateHealth();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            weaponManager.ActivateCurrentWeapon();
        }
    }


    public void ActivateWeapon()
    {
        //weaponManager.ActivateCurrentWeapon();
    }

    public void AnimationEvent()
    {
        weaponManager.currentWeapon.AnimationEvent(weaponManager);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.Equals(GameObject.FindGameObjectWithTag("Exit")))
        {
            if (GameManager.Instance.tutorial == false)
            {
                DynamicLevelGenerator.Instance.Spawn();
            }
            else
            {
                GameManager.Instance.ExitGame();
            }
        }

        if (other.gameObject.transform.tag == "Enemies")
        {
            other.GetComponent<Enemy>().Attack();
            RemoveHealth(10);
            playerHealth.value = calculateHealth();
        }
    }


    public float GetHealth()
    {
        return health;
    }

    public void RemoveHealth(float damage)
    {
        health -= damage;

        if (health <= 0)
        {
            GameManager.Instance.ExitGame();
        }
    }

    private float calculateHealth()
    {
        return GetHealth() / 100;
    }
}
