﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeaponManagerNew : PlayerWeaponManager
{
    public WeaponSlot backSlot;
    public WeaponSlot rightHand;

    public BaseWeapon currentWeapon;

    private Dictionary<WeaponSlotType, WeaponSlot> inventorySlots;

    // Start is called before the first frame update
    void Start()
    {
        //Set up weapon slot dictionary
        inventorySlots = new Dictionary<WeaponSlotType, WeaponSlot>();
        inventorySlots.Add(WeaponSlotType.LARGE_WEAPON, backSlot);
    }

    // Update is called before the every frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad7))
        {
            SwapToWeaponOfType(WeaponSlotType.BELT);
        }
        if (Input.GetKeyDown(KeyCode.Keypad8))
        {
            SwapToWeaponOfType(WeaponSlotType.LARGE_WEAPON);
        }
        if (Input.GetKeyDown(KeyCode.Keypad9))
        {

        }
    }

    public void ActivateCurrentWeapon()
    {
        if (currentWeapon != null)
        {
            currentWeapon.Activate(this);
        }
    }

    public void DropCurrentWeapon()
    {
        Debug.Log("WPNPCK: PlayerWeaponManager: DropCurrentWeapon()");
        Debug.Log("WPNPCK: PlayerWeaponManager: DropCurrentWeapon(): Trying to clear slot ownership");

        //Find the inventory slot that has the current weapon as the owner
        WeaponSlot currentWeaponsSlot = null;
        foreach (KeyValuePair<WeaponSlotType, WeaponSlot> weaponSlotPair in inventorySlots)
        {
            Debug.Log("WPNPCK: PlayerWeaponManager: DropCurrentWeapon(): Checking Slot: " + weaponSlotPair.Value.gameObject.name + ")");

            //if the slot has some owner
            if(weaponSlotPair.Value.SlotHasOwner())
            {
                Debug.Log("WPNPCK: PlayerWeaponManager: DropCurrentWeapon(): Slot has owner");

                if (weaponSlotPair.Value.GetSlotOwner() == currentWeapon)
                {
                    currentWeaponsSlot = weaponSlotPair.Value;
                }
            }

        }
        //Clear slot ownership
        if (currentWeaponsSlot != null)
        {
            Debug.Log("WPNPCK: PlayerWeaponManager: DropCurrentWeapon(): Trying to clear slot ownership (Slot: " + currentWeaponsSlot.gameObject.name + ")");
            currentWeaponsSlot.ClearSlotOwner();
        }
        else
        {
            Debug.Log("WPNPCK: PlayerWeaponManager: DropCurrentWeapon(): Somehow current weapon didn't have a slot");
        }

        currentWeapon.transform.SetParent(null);
        currentWeapon = null;
    }

    public void PickUpWeapon(BaseWeapon weapon)
    {
        weapon.GetComponent<Collider>().isTrigger = true;

        Debug.Log("WPNPCK: PlayerWeaponManager: PickUpWeapon()");

        WeaponSlot availableSlot = GetEmptySlotForWeapon(weapon);

        //If there's an available slot (Current weapon in hand still takes up a slot)
        if(availableSlot != null)
        {
            Debug.Log("WPNPCK: PlayerWeaponManager: PickUpWeapon(): Assigning to slot");
            availableSlot.SetSlotOwner(weapon);
            MoveWeaponToSlot(weapon, availableSlot);

            //If there's no weapon in hand
            if (currentWeapon == null)
            {
                Debug.Log("WPNPCK: PlayerWeaponManager: PickUpWeapon(): Hand is empty, so equipping weapon");
                SetWeaponToCurrent(weapon);
            }

            //TODO: Incorporate the detector into BasePickUpAble
            weapon.GetComponent<EquippableItemPickUpDetector>().mainCollider.enabled = false;
            weapon.GetComponent<EquippableItemPickUpDetector>().pickUpColliderRelay.GetComponent<Collider>().enabled = false;
        }
        else
        {
            Debug.Log("WPNPCK: PlayerWeaponManager: PickUpWeapon(): No room for weapon");
        }


    }

    private void SetWeaponToCurrent(BaseWeapon weapon)
    {
        Debug.Log("WPNPCK: PlayerWeaponManager: EquipWeapon()");
        //Check to make sure weapon is in inventory
        bool weaponFoundInInventory = false;
        foreach(KeyValuePair<WeaponSlotType, WeaponSlot> weaponSlotPair in inventorySlots)
        {
            //If weapon is a match
            if(weaponSlotPair.Value.GetComponentInChildren<BaseWeapon>() == weapon)
            {
                weaponFoundInInventory = true;
            }
        }

        Debug.Log("WPNPCK: PlayerWeaponManager: EquipWeapon(): weaponFoundInInventory = " + weaponFoundInInventory);

        if (weaponFoundInInventory)
        {
            MoveWeaponToSlot(weapon, rightHand);
            currentWeapon = weapon;
            //Call the equip function on the weapon to trigger any weapn specific equip behaviour
            weapon.OnEquip();
        }
    }

    private void MoveCurrentWeaponToInventory()
    {
        if(currentWeapon != null)
        {
            Debug.Log("WPNPCK: PlayerWeaponManager: UnequipCurrentWeapon()");

            //Find the current weapon's owned slot
            WeaponSlot currentWeaponsOwnedSlot = null;
            foreach (KeyValuePair<WeaponSlotType, WeaponSlot> weaponSlotPair in inventorySlots)
            {
                if (weaponSlotPair.Value.GetSlotOwner() == currentWeapon)
                {
                    currentWeaponsOwnedSlot = weaponSlotPair.Value;
                    break;
                }
            }

            //if the an owned slot was found
            if (currentWeaponsOwnedSlot != null)
            {
                Debug.Log("WPNPCK: PlayerWeaponManager: UnequipCurrentWeapon(): Found current weapon's owned slot");
                MoveWeaponToSlot(currentWeapon, currentWeaponsOwnedSlot);
                //Call the unequip function on the weapon to trigger any weapn specific equip behaviour
                currentWeapon.OnUnequip();
                currentWeapon = null;
            }
            else
            {
                Debug.Log("WPNPCK: PlayerWeaponManager: UnequipCurrentWeapon(): Somehow current weapon doesn't have an owned slot!");
            }
        }
    }

    private void MoveWeaponToSlot(BaseWeapon weapon, WeaponSlot slot)
    {
        weapon.gameObject.transform.SetParent(slot.transform);
        weapon.gameObject.transform.localPosition = Vector3.zero;
        weapon.gameObject.transform.localRotation = Quaternion.identity;
    }

    private WeaponSlot GetEmptySlotForWeapon(BaseWeapon weapon)
    {
        Debug.Log("PlayerWeaponManager: GetEmptySlotForWeapon()");

        if (inventorySlots.ContainsKey(weapon.slotType))
        {
            Debug.Log("PlayerWeaponManager: GetEmptySlotForWeapon(): weaponSlots.ContainsKey(weapon.slotType)");
            //If there's nothing in the slot
            if (inventorySlots[weapon.slotType].SlotHasOwner() == false)
            {
                Debug.Log("PlayerWeaponManager: GetEmptySlotForWeapon(): Nothing in the slot");
                //Return the slot
                return inventorySlots[weapon.slotType];
            }
            else
            {
                Debug.Log("PlayerWeaponManager: GetEmptySlotForWeapon(): Slot was occupied");
                return null;
            }
        }
        else
        {
            Debug.Log("PlayerWeaponManager: Tried to add a weapon with a slot type that doesn't exist");
        }

        return null;
    }

    //Empties hands if another weapon isn't found
    public void SwapToWeaponOfType(WeaponSlotType weaponSlotType)
    {
        //Get weapon of slot type in slot if available
        BaseWeapon weaponToEquip = inventorySlots[weaponSlotType].GetComponentInChildren<BaseWeapon>();


        MoveCurrentWeaponToInventory();
        //If a weapon was found of the right type, switches to it
        if (weaponToEquip != null)
        {
            Debug.Log("WPNPCK: PlayerWeaponManager: TrySwapToWeaponOfType(): A weapon of the right type was found, equipping");
            SetWeaponToCurrent(weaponToEquip);
        }
        else
        {
            Debug.Log("WPNPCK: PlayerWeaponManager: TrySwapToWeaponOfType(): No weapon of right type found");
        }

        Debug.Log("WPNPCK: PlayerWeaponManager: TrySwapToWeaponOfType(): Unequipping weapon");
        

    }
}
