﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecurityCameras : MonoBehaviour
{
    public Camera[] cameras = new Camera[5];
    public bool changeAudioListener = true;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("3"))
        {
            EnableCamera(cameras[0], true);
            EnableCamera(cameras[1], false);
            EnableCamera(cameras[2], false);
            EnableCamera(cameras[3], false);
            EnableCamera(cameras[4], false);
        }
        else if (Input.GetKeyDown("4"))
        {
            EnableCamera(cameras[0], false);
            EnableCamera(cameras[1], true);
            EnableCamera(cameras[2], false);
            EnableCamera(cameras[3], false);
            EnableCamera(cameras[4], false);

        }
        else if (Input.GetKeyDown("5"))
        {
            EnableCamera(cameras[0], false);
            EnableCamera(cameras[1], false);
            EnableCamera(cameras[2], true);
            EnableCamera(cameras[3], false);
            EnableCamera(cameras[4], false);

        }
        else if (Input.GetKeyDown("6"))
        {
            EnableCamera(cameras[0], false);
            EnableCamera(cameras[1], false);
            EnableCamera(cameras[2], false);
            EnableCamera(cameras[3], true);
            EnableCamera(cameras[4], false);

        }else if (Input.GetKeyDown("7"))
        {
            EnableCamera(cameras[0], false);
            EnableCamera(cameras[1], false);
            EnableCamera(cameras[2], false);
            EnableCamera(cameras[3], false);
            EnableCamera(cameras[4], true);

        }
    }

    private void EnableCamera(Camera cam, bool enabledStatus)
    {
        cam.enabled = enabledStatus;
        if (changeAudioListener)
            cam.GetComponent<AudioListener>().enabled = enabledStatus;
    }
}
